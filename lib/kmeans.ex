defmodule Kmeans do
  defdelegate sort(centroids, data, loop_count, dist, mean),
    to: Kmeans.KmeansCommand, as: :execute
end
