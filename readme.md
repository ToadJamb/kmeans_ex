Kmeans
======

Calculate Kmeans for an arbitrary set of data.

You supply the method, we supply the means.


Installation
------------

Add `kmeans` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:kmeans, "~> 0.1.0"}
  ]
end
```

Usage
-----

Supply the initial centroids, a distance function, and a mean function
and Kmeans does the rest!

```
defmodule Data
  def data do
    [{
      value: 10,
    }, {
      value: 20,
    }, {
      value: 11,
    }]
  end

  def centroids do
    [{
      value: 19,
    }, {
      value: 12,
    }]
  end

  def distance(a, b) do
    abs a.value - b.value
  end

  def mean(centroids) do
    count = Enum.count(centroids)

    sum =
      centroids
      |> Enum.map(&(&1.value))
      |> Enum.sum

    %{value: sum / count}
  end
end

Kmeans.sort Data.centroids, Data.data, 4, &Data.distance/2, &Data.mean/1
#=> [%{id: 2, value: 10.5}, %{id: 1, value: 20.0}]
```

Contributors
------------

This project would not be where it is (or likely not even exist)
without contribution from [Gregory Ostermayr][gregory-ostermayr].

[gregory-ostermayr]: https://github.com/gregors
