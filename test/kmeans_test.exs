defmodule Core.KmeansTest do
  use ExUnit.Case, async: true

  @described_module Kmeans

  defmodule KmeansTestHelper do
    def to_element(value) do
      %{value: value}
    end

    def to_centroids(values) do
      values
        |> Enum.with_index
        |> Enum.map(&to_centroid/1)
    end

    def to_centroid({value, index}) do
      to_element(value)
      |> Map.put(:id, index + 1)
    end

    def distance(a, b) do
      abs a.value - b.value
    end

    def centroid_mean(centroids) do
      count = Enum.count(centroids)

      sum =
        centroids
        |> Enum.map(fn(centroid) -> centroid.value end)
        |> Enum.sum

      to_element sum / count
    end
  end

  describe ".sort/5" do
    [
      {[10, 20, 11], [8, 6], 3, [13 + 2 / 3, 6]},
      {[10, 20, 11], [8, 8], 3, [20, 10.5]},
      {[10, 20, 11], [10, 4], 3, [13 + 2 / 3, 4]},
    ] |> Enum.each(fn(args) ->
      {data, centroids, count, expected} = args
      params = Macro.escape(args)

      desc = "given data #{inspect(data)} "
      desc = desc <> "given centroids #{inspect(centroids)} "
      desc = desc <> "given #{count} loops "
      desc = desc <> "returns #{inspect(expected)} "

      test desc do
        {data, centroid_values, count, expected_values} = unquote(params)

        list      = Enum.map(data, &KmeansTestHelper.to_element/1)
        centroids = KmeansTestHelper.to_centroids(centroid_values)
        expected  = KmeansTestHelper.to_centroids(expected_values)

        input = [
          centroids,
          list,
          count,
          &KmeansTestHelper.distance/2,
          &KmeansTestHelper.centroid_mean/1,
        ]

        actual = Kernel.apply(@described_module, :sort, input)
                 |> Enum.reverse

        assert actual == expected
      end
    end)
  end
end
