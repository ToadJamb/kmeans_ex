defmodule Kmeans.MixProject do
  use Mix.Project

  def project do
    [
      app: :kmeans,
      version: "0.1.0",
      description: description(),
      package: package(),
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      aliases: aliases(),
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp aliases do
    [
      default: [
        "cmd mix compile --force",
        "cmd mix test --color",
        "cmd mix credo list --strict",
      ],
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"},
      {:ex_doc, only: :dev},

      {:credo, "~> 0.9.1", only: [:dev, :test], runtime: false},
    ]
  end

  defp description() do
    "Calculate Kmeans for an arbitrary set of data."
  end

  defp package() do
    [
      links: links(),
      licenses: licenses(),
    ]
  end

  def licenses() do
    [
      "LGPLV3",
    ]
  end

  def links() do
    %{
      bitbucket: "https://bitbucket.org/toadjamb/kmeans_ex",
    }
  end
end
